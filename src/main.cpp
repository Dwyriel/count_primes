#include <iostream>
#include <string>
#include <vector>
#include <Stopwatch/stopwatch.h>
#include <Logger/logger.h>
#include <ThreadPool/threadpool.h>
#include <DArgumentParser/DArgumentParser.h>
#include <cmake.h>

uint64_t max = 1000000;
Stopwatch stopwatch;

template<typename T>
void EratosthenesST() {
    stopwatch.start();
    uint64_t prime_counter = 1;
    std::vector<T> prime_counting(max);
    prime_counting[0] = true;
    prime_counting[1] = true;
    for (uint64_t i = 4; i < max; i += 2)
        prime_counting[i] = true;
    for (uint64_t i = 3; i < max; i += 2) {
        if (prime_counting[i])
            continue;
        for (uint64_t j = i * 3, it2 = i * 2; j < max; j += it2)
            prime_counting[j] = true;
    }
    for (uint64_t i = 3; i < max; i += 2)
        prime_counter += !prime_counting[i];
    stopwatch.stop();
    const std::string funcName(__PRETTY_FUNCTION__);
    Logger::LogInfo("Function " + funcName);
    Logger::LogInfo("Prime numbers found: " + std::to_string(prime_counter));
    Logger::LogInfo("Time elapsed: " + std::to_string(stopwatch.elapsedMilliseconds()) + "ms");
}

template<typename T>
void EratosthenesMT() {
    stopwatch.start();
    ThreadPool thread_pool;
    uint64_t prime_counter = 1;
    std::vector<T> prime_counting(max);
    prime_counting[0] = true;
    prime_counting[1] = true;
    thread_pool.QueueJob([&prime_counting] {
        const uint64_t max_half = max / 2;
        for (uint64_t i = 4; i < max_half; i += 2)
            prime_counting[i] = true;
    });
    thread_pool.QueueJob([&prime_counting] {
        const uint64_t max_half = max / 2;
        uint64_t i = max_half % 2 == 1 ? max_half - 1 : max_half;
        if (i < 4)
            return;
        for (; i < max; i += 2)
            prime_counting[i] = true;
    });
    thread_pool.QueueJob([&prime_counting] {
        for (uint64_t i = 9; i < max; i += 6)
            prime_counting[i] = true;
    });
    thread_pool.QueueJob([&prime_counting] {
        for (uint64_t i = 15; i < max; i += 10)
            prime_counting[i] = true;
    });
    thread_pool.QueueJob([&prime_counting] {
        for (uint64_t i = 49; i < max; i += 14)
            prime_counting[i] = true;
    });
    thread_pool.Join();
    for (uint64_t i = 11; i < max; i += 2) {
        if (prime_counting[i])
            continue;
        for (uint64_t j = i * 3, it2 = i * 2; j < max; j += it2)
            prime_counting[j] = true;
    }
    for (uint64_t i = 3; i < max; i += 2)
        prime_counter += !prime_counting[i];
    stopwatch.stop();
    const std::string funcName(__PRETTY_FUNCTION__);
    Logger::LogInfo("Function " + funcName);
    Logger::LogInfo("Prime numbers found: " + std::to_string(prime_counter));
    Logger::LogInfo("Time elapsed: " + std::to_string(stopwatch.elapsedMilliseconds()) + "ms");
}

int main(int argc, char *argv[], char *envp[]) {
    Logger::DisableTimeLogging();
    DArgumentParser parser(argc, argv, PROJECT_NAME, PROJECT_VER, "Counts how many prime numbers there are up to [number]");
    DArgumentOption helpOption(DArgumentOptionType::HelpOption, {'h'}, {"help"}, "Prints this menu");
    DArgumentOption versionOption(DArgumentOptionType::VersionOption, {'v'}, {"version"}, "Prints the version");
    DArgumentOption singleThreadedOption({}, {"st"}, "Runs the program on the main thread");
    parser.AddArgumentOption({helpOption, versionOption, singleThreadedOption});
    parser.AddPositionalArgument("number", "The last number to be checked. Default is " + std::to_string(max), "[number]");
    if (parser.Parse() != DParseResult::ParseSuccessful) {
        Logger::LogError(parser.ErrorText());
        exit(EXIT_FAILURE);
    }
    if (parser.GetPositionalArguments().size() > 1) {
        Logger::LogError("Too many arguments passed");
        exit(EXIT_FAILURE);
    }
    if (helpOption.WasSet() || versionOption.WasSet()) {
        if (versionOption.WasSet())
            std::cout << parser.VersionText();
        if (helpOption.WasSet())
            std::cout << parser.HelpText();
        exit(EXIT_SUCCESS);
    }
    if (!parser.GetPositionalArguments().empty()) {
        auto &posArg = parser.GetPositionalArguments()[0];
        try {
            max = std::stoull(posArg);
            if (max < 2) {
                Logger::LogError("Number must bigger than 1");
                return 1;
            }
        } catch (...) {
            Logger::LogError("Argument must be a number");
            return 1;
        }
    }
    Logger::LogInfo("Checking primes up to number " + std::to_string(max++));
    if (singleThreadedOption.WasSet()) {
        Logger::LogInfo("Running single threaded");
        Logger::LogInfo("");
        EratosthenesST<bool>();
        Logger::LogInfo("");
        EratosthenesST<uint8_t>();
    } else {
        Logger::LogInfo("");
        EratosthenesMT<bool>();
        Logger::LogInfo("");
        EratosthenesMT<uint8_t>();
    }
    return 0;
}
